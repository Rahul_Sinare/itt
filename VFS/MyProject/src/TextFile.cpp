#pragma once
#include"TextFile.h"

TextFile::TextFile(){

}
void TextFile::create(const std::string & nameOfFile){
	mName = nameOfFile;
	mTypeOfComponents = 3;
	mSize = 0;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
}

void TextFile::display(){
	log(mName+"\t");
}
bool TextFile::findSizeOfFileSystem(const long &size){
	if (mSize == size)
		return true;
	return false;
}
TextFile::~TextFile(){
	log("Text destructor");
}
bool TextFile::writeFileSystemData(const std::string &fileData){
	this->fileData = fileData;
	mSize=fileData.size();
	return true;
}
const std::string TextFile::readFileData(){
	return fileData;
}
bool TextFile::updateFileSystemName(const std::string& newFileName){
	mName = newFileName;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
	return true;
}
unsigned char TextFile::typeOfFile(){
	return mTypeOfComponents;
}
bool TextFile::findTimestampOfFileSystem(const std::string&time){
	if(mTimestamp == time){
		return true;
	}
	return false;
}
std::vector<std::string>  TextFile::searchDataIntoFile(const std::string& pattern){
		std::istringstream ss(fileData);
		std::string line{};
		std::vector<std::string>listOfLines{};
		std::vector<std::string>resultOfLines{};
		while(getline(ss,line,'\n'))
			listOfLines.push_back(line);
		for(auto line:listOfLines){
			size_t found = line.find(pattern);
			if(found != std::string::npos)
				resultOfLines.push_back(line);
		}
		return resultOfLines;
}