#pragma once
#include"Grep.h"

Grep::Grep(){

}
Grep::~Grep(){

}

void Grep::execcuteCommand(CommandArgument &commandArgument){
    if(commandArgument.getCommandData()->size()!=3)
		throw "use:grep pattern file_name";
	auto listOflineOutputs=commandArgument.getsFileSystemObj()->top()->searchDataIntoFile((*commandArgument.getCommandData())[1],(*commandArgument.getCommandData())[2]);
	for(auto line:listOflineOutputs)
		log(line);
}