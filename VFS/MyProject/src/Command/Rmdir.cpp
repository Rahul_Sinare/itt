#pragma once
#include"Rmdir.h"

Rmdir::Rmdir(){
	
}
Rmdir::~Rmdir(){

}

void Rmdir::execcuteCommand(CommandArgument& commandArgument){
    if(commandArgument.getCommandData()->size()==3){
		if((*commandArgument.getCommandData())[1]!="-r")
			throw "use:mkdir -r directory_name";
		unsigned char num=commandArgument.getsFileSystemObj()->top()->checkTypeOfFileSystem((*commandArgument.getCommandData())[2]);
		if(num==1 && (*commandArgument.getCommandData())[0]=="rmdir"){
				commandArgument.getsFileSystemObj()->top()->deleteDirectoryWithoutEmpty((*commandArgument.getCommandData())[2]);
		}
		return ;		
	}
	unsigned char num=commandArgument.getsFileSystemObj()->top()->checkTypeOfFileSystem((*commandArgument.getCommandData())[1]);
	if(num==0)
		throw "directory or file is not found";

	if(num==1 && (*commandArgument.getCommandData())[0]=="rmdir"){
		if(!commandArgument.getsFileSystemObj()->top()->deleteFileSystem((*commandArgument.getCommandData())[1]))
			throw "Failed to delete directory";
	}
	if(num>=2 &&(*commandArgument.getCommandData())[0]=="rm"){
		if(!commandArgument.getsFileSystemObj()->top()->deleteFileSystem((*commandArgument.getCommandData())[1]))
			throw "Failed to delete file";
	}
}