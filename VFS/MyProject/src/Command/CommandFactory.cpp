#pragma once
#include"CommandFactory.h"
#include"Command.h"
#include"Mkdir.h"
#include"Cat.h"
#include"Cd.h"
#include"Pwd.h"
#include"Rmdir.h"
#include"Find.h"
#include"Ls.h"
#include"Grep.h"
#include"Mv.h"
#include"Touch.h"
#include"WrongCommand.h"

std::shared_ptr<Command> CommandFactory::getCommand(CommandArgument &commandArgument){
    if((*commandArgument.getCommandData())[0] == "mkdir")
        return std::shared_ptr<Command>(new Mkdir);
    if((*commandArgument.getCommandData())[0] == "cd")
        return std::shared_ptr<Command>(new Cd);
    if((*commandArgument.getCommandData())[0] == "find")
        return std::shared_ptr<Command>(new Find);
    if((*commandArgument.getCommandData())[0] == "cat")
        return std::shared_ptr<Command>(new Cat);
    if((*commandArgument.getCommandData())[0] == "pwd")
        return std::shared_ptr<Command>(new Pwd);
    if((*commandArgument.getCommandData())[0] == "rmdir" || (*commandArgument.getCommandData())[0] == "rm")
        return std::shared_ptr<Command>(new Rmdir);
    if((*commandArgument.getCommandData())[0] == "ls")
        return std::shared_ptr<Command>(new Ls);
    if((*commandArgument.getCommandData())[0] == "mv")
        return std::shared_ptr<Command>(new Mv);
    if((*commandArgument.getCommandData())[0] == "touch"||(*commandArgument.getCommandData())[0] == "vi")
        return std::shared_ptr<Command>(new Touch);
    if((*commandArgument.getCommandData())[0] == "grep")
        return std::shared_ptr<Command>(new Grep);
    if(1)
        return std::shared_ptr<Command>(new WrongCommand);
}