#pragma once
#include"Cd.h"

Cd::Cd(){
}
Cd::~Cd(){

}
bool Cd::backToParentDirectory(CommandArgument &commandArgument){
	
	if(commandArgument.getsFileSystemObj()->size()!=1){
		commandArgument.getsFileSystemObj()->pop();
		commandArgument.getDirectoryPath()->pop_back();
		return true;
	}
	return false;
}

void Cd::enteringIntoDirectory(CommandArgument& commandArgument){
	std::vector<std::string> listOfDirectory=splitPathName((*commandArgument.getCommandData())[1]);
	for(auto &directoryName:listOfDirectory){
		std::shared_ptr<FileSystem>tempFileSystem{nullptr};
		if((tempFileSystem=commandArgument.getsFileSystemObj()->top()->changeDirectory(directoryName))!=nullptr){
			commandArgument.getsFileSystemObj()->push(tempFileSystem);
			commandArgument.getDirectoryPath()->push_back(directoryName);
		}   
	}
}
void Cd::execcuteCommand(CommandArgument& commandArgument){
    if((*commandArgument.getCommandData()).size()!=2)
		throw "use: cd directory_name";
	if((*commandArgument.getCommandData())[1] ==".."){
		backToParentDirectory(commandArgument);
		return ;
	}
	enteringIntoDirectory(commandArgument);
}
