#pragma once
#include"Mv.h"

Mv::Mv(){
	
}
Mv::~Mv(){

}

void Mv::execcuteCommand(CommandArgument &commandArgument){
    if(commandArgument.getCommandData()->size()!=3){
		throw "use: mv file_name new_file_name";
	}
	if(!commandArgument.getsFileSystemObj()->top()->updateFileSystemName((*commandArgument.getCommandData())[1],(*commandArgument.getCommandData())[2]))
		throw "Failed to update";
}