#pragma once 
#include"CommandArgument.h"

void CommandArgument::setsFileSystemObj(std::stack<std::shared_ptr<FileSystem>> *psFileSystemObj)
{
    sFileSystemObj = psFileSystemObj;
}
void CommandArgument::setDirectoryPath(std::vector<std::string> *pDirectoryPath)
{
    directoryPath = pDirectoryPath;
}
void CommandArgument::setCommandData(std::vector<std::string> *pCommandData)
{
    commandData = pCommandData;
}

std::stack<std::shared_ptr<FileSystem>> *CommandArgument::getsFileSystemObj()
{
    return sFileSystemObj;
}
std::vector<std::string> *CommandArgument::getDirectoryPath()
{
    return directoryPath;
}
std::vector<std::string> *CommandArgument::getCommandData()
{
    return commandData;
}

CommandArgument::~CommandArgument()
{
}
