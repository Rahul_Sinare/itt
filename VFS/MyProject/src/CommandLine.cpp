#pragma once
#include<queue>
#include<sstream>
#include<stack>
#include<vector>
#include"ColorCode.h"
#include"FileSystem.h"
#include"CommandFactory.h"

void createPath(std::vector<std::string> directoryPath,std::string &directory){
	
	for(auto directoryName:directoryPath)
		directory += "/" + directoryName;
}
std::vector<std::string> splitSpace(const std::string &command){
	std::vector<std::string>result{};
	std::istringstream ss(command);
	std::string word{};
	while(ss >> word)
		result.push_back(word);
	return result;
}


void parseCommand(std::stack<std::shared_ptr<FileSystem>>&sFileSystemObj,std::vector<std::string>&directoryPath,const std::string &parseCommandInternal)
{
	auto commandData = splitSpace(parseCommandInternal);
	try{
		CommandArgument commandArgument;
		commandArgument.setsFileSystemObj(&sFileSystemObj);
		commandArgument.setDirectoryPath(&directoryPath);
		commandArgument.setCommandData(&commandData);
		std::shared_ptr<Command> ICommand = CommandFactory::getCommand(commandArgument);
		ICommand->execcuteCommand(commandArgument);
	}
	catch(const char *error){
		log(red+error+reset);
	}
	catch(std::bad_alloc &badAllocatonMemory){
		log(badAllocatonMemory.what());
	}
	
}

void commandLine(){
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};
	std::stack<std::shared_ptr<FileSystem>> sFileSystemObj{};
	std::vector<std::string>directoryPath{};
	sFileSystemObj.push(fileSystemObj);
	directoryPath.push_back("root");
	std::string userName{"rahulss@ITT:~"};

	while(1){
		std::string command{};
		std::string dir{};
		createPath(directoryPath,dir);
		logInit(green+userName+reset+blue+dir+reset+"$");
		getline(std::cin,command);
		parseCommand(sFileSystemObj,directoryPath,command);
		logNewLine();
	}
}
