#pragma once
#include"Directory.h"
#include"ColorCode.h"
Directory::Directory(){

}
void Directory::create(const std::string & nameOfDirectory){
	mName = nameOfDirectory;
	mTypeOfComponents = 1;
	mSize = 0;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
	newFileSystemObj = std::shared_ptr<FileSystem>(new FileSystem);
}

void Directory::display(){
	log(blue+mName+reset+"\t");
}
Directory::~Directory(){
	log("Directory destructor");
}
std::shared_ptr<FileSystem> Directory::getDirectory(){
	return newFileSystemObj;
}
bool Directory::findSizeOfFileSystem(const long &size){
	if(mSize == size)
		return true;
	return false;
}
bool Directory::updateFileSystemName(const std::string& newDirectoryName){
	mName = newDirectoryName;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
	return true;
}
unsigned char Directory::typeOfFile(){
	return mTypeOfComponents;
}
bool Directory::findTimestampOfFileSystem(const std::string&time){
	if(mTimestamp == time)
		return true;
	return false;
}
