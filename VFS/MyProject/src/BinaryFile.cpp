#pragma once
#include"BinaryFile.h"
BinaryFile::BinaryFile(){

}
void BinaryFile::create(const std::string & nameOfDir){
	mName = nameOfDir;
	mTypeOfComponents = 2;
	mSize = 0;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
}

BinaryFile::~BinaryFile(){
	log("Binary destructor");
}
void BinaryFile::display(){
	log(mName+"\t");
}
bool BinaryFile::findSizeOfFileSystem(const long &size){
	if(mSize == size)
		return true;
	return false;
}
bool BinaryFile::writeFileSystemData(const std::string &fileData){
	this->fileData = fileData;
	mSize = fileData.size();
	return 0;
}
const std::string BinaryFile::readFileData(){
	return fileData;
}
bool BinaryFile::updateFileSystemName(const std::string& newFsName){
	mName = newFsName;
	auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	mTimestamp = ctime(&timenow);
	return true;
}
unsigned char BinaryFile::typeOfFile(){
	return mTypeOfComponents;
}
bool BinaryFile::findTimestampOfFileSystem(const std::string&time){
	if(mTimestamp == time)
		return true;
	return false;
}
std::vector<std::string> BinaryFile::searchDataIntoFile(const std::string& substring){
	std::istringstream ss(fileData);
	std::string line{};
	std::vector<std::string>listOfLines{};
	std::vector<std::string>resultOfLines{};
	while(getline(ss,line,'\n'))
		listOfLines.push_back(line);
	for(auto line:listOfLines){
		size_t found = line.find(substring);
		if(found != std::string::npos)
			resultOfLines.push_back(line);
	}
	return resultOfLines;
}
