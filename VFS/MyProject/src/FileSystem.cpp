
#pragma once
#include<memory>
#include<sstream>
#include<vector>
#include"FileSystemComponents.h"
#include"FileSystem.h"
#include"Directory.h"
#include"TextFile.h"
#include"BinaryFile.h"
std::vector<std::string> splitPathName( std::string namedirectory){
	std::vector<std::string>result;
	std::istringstream ss(namedirectory);
	std::string directory{};
	while(getline(ss,directory,'/'))
		result.push_back(directory);
	return result;
}
bool isPath(const std::string &nameOfDirectory){
	size_t found = nameOfDirectory.find("/");
	if(found == std::string::npos)
		return false;
	return true;
}
std::shared_ptr<FileSystem> FileSystem::findPathToCreate(const std::string &nameOfDirectory, std::string& newDirectory){
	auto listOfDirectoryName = splitPathName(nameOfDirectory);

	std::shared_ptr<FileSystem> tempFileSystem = nullptr;
	auto directoryIterator = listOfFS.find(listOfDirectoryName[0]);
	if(directoryIterator == listOfFS.end())
			throw "Directory path not found";
		//listOfDirName.erase(listOfDirName.begin());
		tempFileSystem = directoryIterator->second->getDirectory();
		for(int position = 1; position<listOfDirectoryName.size()-1; ++position){
			auto foundDirectory = tempFileSystem->listOfFS.find(listOfDirectoryName[position]);
			if(foundDirectory == tempFileSystem->listOfFS.end())
					throw "Directory path not found";
				tempFileSystem = foundDirectory->second->getDirectory();
		}
			newDirectory = listOfDirectoryName[listOfDirectoryName.size()-1];
			return tempFileSystem;
}
bool FileSystem::isFileAlreadyExit(const std::string &nameofFile)
{
	auto iterator = listOfFS.find(nameofFile);
	if(iterator != listOfFS.end()){
		throw "Already exist";
	}
	return false;
}

bool FileSystem::createDirectory(const std::string& nameOfDirectory){
	if(isPath(nameOfDirectory)){
		std::string newDirectory{};
		std::shared_ptr<FileSystem>tempFileSystem = findPathToCreate(nameOfDirectory,newDirectory);
			return tempFileSystem->createDirectory(newDirectory);
	}
	if(isFileAlreadyExit(nameOfDirectory))
		return false;
	std::shared_ptr<FileSystemComponents> IfileSystemComponents_{new Directory};
	IfileSystemComponents_->create(nameOfDirectory);
	listOfFS[nameOfDirectory] = IfileSystemComponents_;
	return true;
}
bool FileSystem::basedOnFileTypeToCreateFile(const std::string &nameOfFile){
	std::shared_ptr<FileSystemComponents>IfileSystemComponents_{nullptr};
	size_t found = nameOfFile.find(".bin");
	if(found != std::string::npos)	
		IfileSystemComponents_ = std::shared_ptr<FileSystemComponents>(new BinaryFile);
	else
		IfileSystemComponents_ = std::shared_ptr<FileSystemComponents>(new TextFile);
	IfileSystemComponents_->create(nameOfFile);
	listOfFS[nameOfFile] = IfileSystemComponents_;
	return true;
}
bool FileSystem::createFile(const std::string &nameOfFile){
	if(isPath(nameOfFile)){
		std::string newFile{};
		std::shared_ptr<FileSystem>tempFileSystem = findPathToCreate(nameOfFile,newFile);
			return tempFileSystem->createFile(newFile);
	}
	if(isFileAlreadyExit(nameOfFile))
		return false;
	return basedOnFileTypeToCreateFile(nameOfFile);	
}
void FileSystem::displayCurrentFS(){
	if(listOfFS.empty()){
		throw "Empty directory";
	}
	for(auto fileSystemComponents:listOfFS){
		fileSystemComponents.second->display();
	}
}
FileSystem::~FileSystem(){
	log("Fille system destructor");
}
std::shared_ptr<FileSystem>  FileSystem::changeDirectory(const std::string &nameOfDirectory){
	auto iterator = listOfFS.find(nameOfDirectory);
	if(iterator != listOfFS.end())
		return iterator->second->getDirectory();
	throw "Directory not found";
}
std::string FileSystem::findNameOfFileSystem(const std::string &nameOfFile){
	auto iterator = listOfFS.find(nameOfFile);
	if(iterator == listOfFS.end()){
		throw "File not found";
	}
	return iterator->first;
}
bool FileSystem::writeFileSystemData(const std::string &fileName,const std::string& fileData){
	auto iterator = listOfFS.find(fileName);
	if(iterator == listOfFS.end())
		throw "File not found";
	iterator->second->writeFileSystemData(fileData);
	return true;
}
bool FileSystem::findSizeOfFileSystem(const long & size){
	for(const auto &fileSystemComponents : listOfFS){
		if(fileSystemComponents.second->findSizeOfFileSystem(size))
			return true;
	}
	throw "File size not found";
}
const std::string FileSystem::readFileSystemData(const std::string &fileName){
	auto iterator = listOfFS.find(fileName);
	if(iterator != listOfFS.end())
		return iterator->second->readFileData();
	throw "File name not found";
}
bool FileSystem::updateFileSystemName(const std::string &nameOfFile,const std::string&newNameOfFile){
	auto iterator = listOfFS.find(nameOfFile);
	if(iterator == listOfFS.end()){
		throw "File not found";
	}
	iterator->second->updateFileSystemName(newNameOfFile);
	std::shared_ptr<FileSystemComponents> tempFileSystem=iterator->second;
	listOfFS.erase(nameOfFile);
	listOfFS[newNameOfFile] = tempFileSystem;
	return true;
}
std::shared_ptr<FileSystem> FileSystem::isProvidedPathIsCorrect(const std::string &nameOfFile,std::string &directoryName_){
	auto listOfDirectoryName = splitPathName(nameOfFile);
		int position{};
		std::shared_ptr<FileSystem>tempFileSystem = nullptr;
		auto iteratorDir = listOfFS.find(listOfDirectoryName[0]);
		if(iteratorDir == listOfFS.end())
			throw "Path not found";
		tempFileSystem = iteratorDir->second->getDirectory();
		for(position = 1;position < listOfDirectoryName.size()-1; ++position){
				auto iteratorDirectory = tempFileSystem->listOfFS.find(listOfDirectoryName[position]);
				if(iteratorDirectory == tempFileSystem->listOfFS.end())
					throw "Path not found";
				tempFileSystem = iteratorDirectory->second->getDirectory();
		}
		directoryName_ = listOfDirectoryName[listOfDirectoryName.size()-1];
		return tempFileSystem;
}
unsigned char FileSystem::checkTypeOfFileSystem(const std::string &nameOfFile){
	if(isPath(nameOfFile)){
		std::string fileName{};
		std::shared_ptr<FileSystem> tempFileSystem=isProvidedPathIsCorrect(nameOfFile,fileName);
		return tempFileSystem->checkTypeOfFileSystem(fileName);
	}	
	auto iterator=listOfFS.find(nameOfFile);
	if(iterator==listOfFS.end())
		throw "File not found";
	if(iterator->second->typeOfFile()==1)
		return 1;
	return 2;
}
bool FileSystem::isDirectoryEmpty(){
	return listOfFS.size()==0;
}
bool FileSystem::deleteFileSystem(const std::string& nameOfFile){
	
	if(isPath(nameOfFile)){
		std::string fileName{};
		std::shared_ptr<FileSystem> tempFilesystem=isProvidedPathIsCorrect(nameOfFile,fileName);
		return tempFilesystem->deleteFileSystem(fileName);
	}
	auto iterator=listOfFS.find(nameOfFile);

	if(iterator==listOfFS.end())
		throw "File not found";
	if(iterator->second->typeOfFile()!=1){
		listOfFS.erase(nameOfFile);
		return true;
	}
	std::shared_ptr<FileSystem> tempFileSystem=iterator->second->getDirectory();
	if(!tempFileSystem->isDirectoryEmpty()){
		throw "Directory is not empty";
	}
	listOfFS.erase(nameOfFile);
	return true;
}
bool FileSystem::deleteDirectoryWithoutEmpty(const std::string& nameOfFile){
	if(isPath(nameOfFile)){
		std::string fileName{};
		std::shared_ptr<FileSystem> tempFileSystem=isProvidedPathIsCorrect(nameOfFile,fileName);
		return tempFileSystem->deleteFileSystem(fileName);
	}
	auto iterator=listOfFS.find(nameOfFile);

	if(iterator==listOfFS.end())
		throw "File not found";
	if(iterator->second->typeOfFile()!=1){
		listOfFS.erase(nameOfFile);
		return true;
	}
	listOfFS.erase(nameOfFile);
	return true;
}
bool FileSystem::findTimestampOfFileSystem(const std::string &time){
	for(const auto &fileSystemComponents:listOfFS){
		if(fileSystemComponents.second->findTimestampOfFileSystem(time))
			return true;
	}
	throw "time stamp not found";
}
std::vector<std::string>FileSystem::searchDataIntoFile(const std::string& pattern,const std::string&fileName){
	auto found=listOfFS.find(fileName);
	if(found==listOfFS.end())
		throw "File not exist";
	return found->second->searchDataIntoFile(pattern);
}
