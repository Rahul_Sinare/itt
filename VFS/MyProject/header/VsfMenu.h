#pragma once
#include<iostream>
#include<unordered_map>
#include<stack>
#include<memory>
#include<string>
#include"../header/FileSystem.h"

void createDirectoryInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string userInput;
	std::cout<<"Please enter directory name to create__"<<std::endl;
	getline(std::cin,userInput,',');
	sObj.top()->createDirectory(userInput);
}
void createFileInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string userInput{};
	std::cout<<"Please enter File name to create__"<<std::endl;
	getline(std::cin,userInput,',');
	sObj.top()->createFile(userInput);
}
void changeDirectoryInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::shared_ptr<FileSystem> tempfs{nullptr};
	std::string userInput;
	std::cout<<"Please enter directory name to change directory__"<<std::endl;
	getline(std::cin,userInput,',');
	if((tempfs=sObj.top()->changeDirectory(userInput))!=nullptr)
		sObj.push(tempfs);
}
void findNameOfFileSystemInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string userInput{};
	std::cout<<"Please enter directory or file name to find__"<<std::endl;
	getline(std::cin,userInput,',');
		std::cout<<sObj.top()->findNameOfFileSystem(userInput);
}
void findSizeOfFileSystemInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	long size{};
	std::cout<<"Please enter directory or file size to find__"<<std::endl;
	std::cin>>size;
	if(sObj.top()->findSizeOfFileSystem(size))
		std::cout<<"Size found"<<std::endl;
}
void writeFileSystemDataInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string fileName{};
	std::string fileData{};
	std::cout<<"Please enter file name__"<<std::endl;
	getline(std::cin,fileName,',');
	std::cout<<"Please enter file data__ "<<std::endl;
	getline(std::cin,fileData,',');
	if(sObj.top()->writeFileSystemData(fileName,fileData))
		std::cout<<"Data writing into file succesfully"<<std::endl;
}
void readFileSystemDataInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string fileName{};
	std::cout<<"Please enter file name__"<<std::endl;
	getline(std::cin,fileName,',');
	std::cout<<sObj.top()->readFileSystemData(fileName)<<std::endl;
}
void updateFileSystemNameInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string fileName{};
	std::string newFileName{};
	std::cout<<"Please enter file name__"<<std::endl;
	getline(std::cin,fileName,',');
	std::cout<<"Please enter new file name__ "<<std::endl;
	getline(std::cin,newFileName,',');
	if(sObj.top()->updateFileSystemName(fileName,newFileName))
		std::cout<<"File system name updated successfully"<<std::endl;
}
void deleteFileSystemInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string fileName{};
	std::cout<<"Please enter file or directory name to delete__"<<std::endl;
	getline(std::cin,fileName,',');
	if(sObj.top()->deleteFileSystem(fileName))
		std::cout<<"Succesfully deleted"<<std::endl;
}
void findTimestampOfFileSystemInput(std::stack<std::shared_ptr<FileSystem>>&sObj){
	std::string userInput{};
	std::cout<<"Please enter directory or file time stamp to find__"<<std::endl;
	getline(std::cin,userInput,',');
	if(sObj.top()->findTimestampOfFileSystem(userInput))
		std::cout<<"Time stamp found"<<std::endl;
}
bool comeBackToDirectory(std::stack<std::shared_ptr<FileSystem>>&sObj){
	if(sObj.size()!=1){
		sObj.pop();
		return true;
	}
	return false;
}
void ProvingMenuOfVFS(){
	std::shared_ptr<FileSystem>fsObj{new FileSystem};
	std::stack<std::shared_ptr<FileSystem>>sObj;
	sObj.push(fsObj);
	int option{};
	while(1){
		std::cout<<std::endl<<"*******************************"<<std::endl;


		std::cout<<"Please select option\n1:Create Directory\n2:Create File\n3:Display Current File System\n4:Change Directory\n13:Come back to the directory\n5:Find Name Of File System\n6:Find Size Of File System\n7:Write File System Data\n8:Read File System Data\n9:Update File System Name\n10:Delete File System\n11:Find Timestamp Of File System\n12:Exit"<<std::endl;

		std::cin>>option;

		switch(option){
			case 1:
				createDirectoryInput(sObj);
				break;
			case 2:
				createFileInput(sObj);
				break;
			case 3:
				sObj.top()->displayCurrentFS();
				break;
			case 4:
				changeDirectoryInput(sObj);
				break;
			case 5:
				findNameOfFileSystemInput(sObj);
				break;
			case 6:
				findSizeOfFileSystemInput(sObj);
				break;
			case 7:
				writeFileSystemDataInput(sObj);
				break;
			case 8:
				readFileSystemDataInput(sObj);
				break;
			case 9:
				updateFileSystemNameInput(sObj);
				break;
			case 10:
				deleteFileSystemInput(sObj);
				break;
			case 11:
				findTimestampOfFileSystemInput(sObj);
				break;
			case 13:
				comeBackToDirectory(sObj);
				break;
			case 12:
				exit(0);
				break;
			default:
				std::cout<<"Invalid Choice"<<std::endl;
		}
	}
}
