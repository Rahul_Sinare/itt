#pragma once
#include"File.h"

class BinaryFile : public File {
	public:
		BinaryFile();
		//BinaryFile(const std::string&);
		void display();
		void create(const std::string&);
		bool findSizeOfFileSystem(const long &);
		bool writeFileSystemData(const std::string &);
		const std::string readFileData();
		bool updateFileSystemName(const std::string& );
		unsigned char typeOfFile();
		bool findTimestampOfFileSystem(const std::string&);
		std::vector<std::string> searchDataIntoFile(const std::string&);
		virtual ~BinaryFile();
};
