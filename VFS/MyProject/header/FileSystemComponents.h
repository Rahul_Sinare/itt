#pragma once
#include<iostream>
#include<string>
#include<chrono>
#include<memory>
#include<sstream>
#include<vector>
#include"log.h"
using namespace std::chrono;

class FileSystem;
class FileSystemComponents {
	public:
		FileSystemComponents();
		virtual void display()=0;
		virtual void create(const std::string&)=0;
		virtual bool findSizeOfFileSystem(const long &)=0;
		virtual bool writeFileSystemData(const std::string &);
		virtual const std::string readFileData();
		virtual bool updateFileSystemName(const std::string&)=0;
		virtual unsigned char typeOfFile()=0;
		virtual bool findTimestampOfFileSystem(const std::string &)=0;
		virtual std::shared_ptr<FileSystem> getDirectory();
		virtual std::vector<std::string> searchDataIntoFile(const std::string&);
		virtual ~FileSystemComponents();
	protected:
		std::string mName;
		unsigned mTypeOfComponents;
		long mSize;
		std::string mTimestamp;
};
