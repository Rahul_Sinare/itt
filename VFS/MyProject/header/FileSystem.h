#pragma once
#include<unordered_map>
#include"log.h"
#include"FileSystemComponents.h"
class FileSystem{
	public:
		bool createDirectory(const std::string& nameOfDir);
		bool createFile(const std::string &nameOfFile);
		void displayCurrentFS();
		std::shared_ptr<FileSystem> changeDirectory(const std::string& );
		std::string findNameOfFileSystem(const std::string &);
		bool findSizeOfFileSystem(const long &);
		bool writeFileSystemData(const std::string &,const std::string &);
		const std::string readFileSystemData(const std::string &);
		bool updateFileSystemName(const std::string &,const std::string& );
		bool deleteFileSystem(const std::string&);
		bool findTimestampOfFileSystem(const std::string &);
		unsigned char checkTypeOfFileSystem(const std::string &);
		bool isFileAlreadyExit(const std::string &);
		bool isDirectoryEmpty();
		bool basedOnFileTypeToCreateFile(const std::string &);
		std::shared_ptr<FileSystem> isProvidedPathIsCorrect(const std::string &, std::string&);
		std::shared_ptr<FileSystem>findPathToCreate(const std::string&, std::string&);
		std::vector<std::string>searchDataIntoFile(const std::string&,const std::string&);
		bool deleteDirectoryWithoutEmpty(const std::string&);
		~FileSystem();
	private:
		std::unordered_map<std::string,std::shared_ptr<FileSystemComponents>>listOfFS;
};





