#pragma once
#include"Command.h"
class Cd : public Command {
	public:
		Cd();
		void execcuteCommand(CommandArgument &);
		bool backToParentDirectory(CommandArgument&);
		void enteringIntoDirectory(CommandArgument&);
		virtual ~Cd();
};
std::vector<std::string> splitPathName( std::string namedir);
