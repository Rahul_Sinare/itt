#pragma once
#include<stack>
#include"../header/FileSystem.h"
#include"Command.h"
class CommandArgument{
    public:
        void setsFileSystemObj(std::stack<std::shared_ptr<FileSystem>> *);
        void setDirectoryPath(std::vector<std::string> *);
        void setCommandData(std::vector<std::string> *);

        std::stack<std::shared_ptr<FileSystem>> *getsFileSystemObj();
        std::vector<std::string> *getDirectoryPath();
        std::vector<std::string> *getCommandData();
        ~CommandArgument();
    private:
        std::stack<std::shared_ptr<FileSystem>> *sFileSystemObj;
        std::vector<std::string> *directoryPath;
        std::vector<std::string> *commandData;
};