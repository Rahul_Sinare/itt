#pragma once
#include<memory>
#include<stack>
#include<vector>
#include"CommandArgument.h"
#include"../header/FileSystem.h"
class CommandArgument;
class Command {
	public:
		Command();
		virtual void execcuteCommand(CommandArgument &)=0;
		virtual ~Command();
};
