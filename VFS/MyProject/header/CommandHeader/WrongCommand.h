#pragma once
#include"Command.h"

class WrongCommand : public Command {
    public:
        WrongCommand();
        void execcuteCommand(CommandArgument &);
        virtual ~WrongCommand();
};