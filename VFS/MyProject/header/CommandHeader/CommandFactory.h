#pragma once
#include"Command.h"
class CommandFactory {
	public:
		static std::shared_ptr<Command> getCommand(CommandArgument&);
};