#pragma once
#include<memory>
#include"FileSystemComponents.h"
#include"FileSystem.h"
class Directory : public FileSystemComponents {
	  public:
		    Directory();
	    	//Directory(const std::string&);
		    void display();
			void create(const std::string&);
	    	std::shared_ptr<FileSystem> getDirectory();
	    	bool findSizeOfFileSystem(const long &);
	    	bool updateFileSystemName(const std::string& );
		    unsigned char typeOfFile();
		    bool findTimestampOfFileSystem(const std::string&);
		    virtual ~Directory();
	  private:
		    std::shared_ptr<FileSystem>newFileSystemObj;
};
