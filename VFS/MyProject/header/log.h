#pragma once
#include<iostream>

void logInit(const std::string&);
void logNewLine();
void log(const char*);
void log(const std::string&);