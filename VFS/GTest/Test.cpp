#pragma once
#include<gtest/gtest.h>
#include<memory>
#include<stack>
#include"FileSystem.h"
#include"VsfMenu.h"

TEST ( CreateDirectoryTest, WhenDirectoryCreated) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFIleSystem = nullptr;

	sFileSystemObj.push(fileSystemObj);

	ASSERT_TRUE (sFileSystemObj.top()->createDirectory(std::string{"College"}));
}

TEST ( CreateFileTest, WhenFileCreated) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	ASSERT_TRUE (sFileSystemObj.top()->createFile(std::string{"rahul.txt"}));
}
TEST ( WriteDataIntoFileTest, whenWriteDataIntoFile) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});
	
    if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);
	
    sFileSystemObj.top()->createFile(std::string{"file.txt"});
	
    ASSERT_TRUE (sFileSystemObj.top()->writeFileSystemData(std::string{"file.txt"},std::string{"My name is Rahul Sinare"}));
}
TEST (ReadDataFromFileTest, whenReadDataFromFile) {
	
    std::shared_ptr<FileSystem>fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	sFileSystemObj.top()->writeFileSystemData(std::string{"file.txt"},std::string{"My name is Rahul Sinare"});

	ASSERT_STREQ ("My name is Rahul Sinare", sFileSystemObj.top()->readFileSystemData(std::string{"file.txt"}).c_str());
}
TEST (UpdateDirNameTest, whenUpdateDirectory) {

	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	ASSERT_TRUE(sFileSystemObj.top()->updateFileSystemName(std::string{"file.txt"},std::string{"File.txt"}));
}
TEST (DeleteDirNameTest, whenDeleteDirectory) {

	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	ASSERT_TRUE (sFileSystemObj.top()->deleteFileSystem(std::string{"file.txt"}));
}
TEST(VsfTest,vsfExcution){

	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	sFileSystemObj.top()->createFile(std::string{"rahul.txt"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createDirectory(std::string{"Branch"});

	sFileSystemObj.top()->createFile(std::string{"sinare.bin"});

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	ASSERT_TRUE ( sFileSystemObj.top()->writeFileSystemData(std::string{"file.txt"},std::string{"My name is Rahul Sinare"}));

	ASSERT_STREQ ("sinare.bin", sFileSystemObj.top()->findNameOfFileSystem(std::string{"sinare.bin"}).c_str());

	ASSERT_STREQ ("My name is Rahul Sinare", sFileSystemObj.top()->readFileSystemData(std::string{"file.txt"}).c_str());

	ASSERT_TRUE (sFileSystemObj.top()->updateFileSystemName(std::string{"Branch"},std::string{"branch"}));

	ASSERT_TRUE (sFileSystemObj.top()->deleteFileSystem(std::string{"sinare.bin"}));

	ASSERT_TRUE ( comeBackToDirectory(sFileSystemObj));

	ASSERT_STREQ ("rahul.txt", sFileSystemObj.top()->findNameOfFileSystem(std::string{"rahul.txt"}).c_str());

	ASSERT_TRUE (sFileSystemObj.top()->updateFileSystemName(std::string{"College"},std::string{"college"}));
}

TEST ( CreateDirectoryTest, WhenDirectoryNotCreated) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFIleSystem = nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});
	
	ASSERT_ANY_THROW (sFileSystemObj.top()->createDirectory(std::string{"College"}));
}


TEST ( CreateFileTest, WhenFileNotCreated) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createFile(std::string{"rahul.txt"});
	ASSERT_ANY_THROW (sFileSystemObj.top()->createFile(std::string{"rahul.txt"}));
}
TEST ( WriteDataIntoFileTest, whenWriteDataNotIntoFile) {
	
	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});
	
    if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);
	
    sFileSystemObj.top()->createFile(std::string{"file.txt"});
	
    ASSERT_ANY_THROW (sFileSystemObj.top()->writeFileSystemData(std::string{"College"},std::string{"My name is Rahul Sinare"}));
}
TEST (ReadDataFromFileTest, whenReadDataNotFromFile) {
	
    std::shared_ptr<FileSystem>fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	sFileSystemObj.top()->writeFileSystemData(std::string{"file.txt"},std::string{"My name is Rahul Sinare"});

	ASSERT_ANY_THROW ( sFileSystemObj.top()->readFileSystemData(std::string{"file.bin"}));
}
TEST (UpdateDirNameTest, whenNotUpdateDirectory) {

	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);

	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	ASSERT_ANY_THROW ( sFileSystemObj.top()->updateFileSystemName(std::string{"file.bin"},std::string{"File.txt"}));
}
TEST (DeleteDirNameTest, whenNotDeleteDirectory) {

	std::shared_ptr<FileSystem> fileSystemObj{new FileSystem};

	std::stack<std::shared_ptr<FileSystem>>sFileSystemObj;

	std::shared_ptr<FileSystem> tempFileSystem=nullptr;

	sFileSystemObj.push(fileSystemObj);

	sFileSystemObj.top()->createDirectory(std::string{"College"});

	if((tempFileSystem=sFileSystemObj.top()->changeDirectory(std::string{"College"}))!=nullptr)
		sFileSystemObj.push(tempFileSystem);


	sFileSystemObj.top()->createFile(std::string{"file.txt"});

	comeBackToDirectory(sFileSystemObj);
	ASSERT_ANY_THROW (sFileSystemObj.top()->deleteFileSystem(std::string{"College"}));
}